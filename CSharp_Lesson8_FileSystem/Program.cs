﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;

namespace CSharp_Lesson8_FileSystem
{
    class Program
    {
        static void Main(string[] args)
        {
            #region GC 
            //SqlConnection sqlConnection = new SqlConnection();
            //sqlConnection.Open();
            ////ToDo code;
            //sqlConnection.Close();

            //using (SqlConnection sqlConnection2 = new SqlConnection())
            //{
            //    sqlConnection2.Open();
            //    //ToDo code;
            //}
            //a тут sqlConnection2 уже не существует и уничтожен в управл и неуправл памяти


            //for (int i = 0; i < 10000000; i++)
            //{
            //    System.Threading.Thread.Sleep(500);
            //    GC.Collect();
            //}
            #endregion

            string fullPath = "";

            Console.WriteLine("Choose logical device(disk)");
            var driveInfo = DriveInfo.GetDrives();

            for (int i = 0; i < driveInfo.Length; i++)
            {
                Console.WriteLine($"{i}. {driveInfo[i].Name}");
            }

            string driveNumberAsString = Console.ReadLine();
            int driveNumber;
            if (int.TryParse(driveNumberAsString, out driveNumber))
            {
                fullPath += driveInfo[driveNumber].Name;
                Console.Clear();

                Console.WriteLine("Entern name of directory with you want write file");
                string directoryName = Console.ReadLine();
                fullPath += directoryName;
                Directory.Exists(fullPath);

                if (!Directory.Exists(fullPath))
                {
                    Directory.CreateDirectory(fullPath);
                }

                Console.WriteLine("Enter file name with type you want to create ");
                string fileName = Console.ReadLine();
                fullPath += "\\" + fileName;

                //if (File.Exists(fullPath))
                //{
                //    Console.WriteLine("file exists. creating file with tmp type");
                //    fullPath += ".temp";
                //}

                //File.Create(fullPath);

                string data = "text data test";

                //FileStream, StreamWriter, StreamReader, BinaryWriter, BinaryReader.

                //using (FileStream stream = new FileStream(fullPath, FileMode.Create))
                //{
                //    byte[] bytesData = Encoding.UTF8.GetBytes(data); //Конвертирует в байты строку (любые данные)
                //    stream.Write(bytesData, 0, bytesData.Length);

                //}

                //using (FileStream stream = File.OpenRead(fullPath) /*new FileStream(fullPath, FileMode.OpenOrCreate)*/)
                //{
                //    byte[] bytesData = new byte[stream.Length];
                //    stream.Read(bytesData, 0, bytesData.Length);

                //    string result = Encoding.UTF8.GetString(bytesData);
                //    Console.WriteLine(result);
                //}

                //using (StreamWriter stream = new StreamWriter(fullPath))
                //{
                //    stream.WriteLine(data);
                //}

                //using (StreamReader stream = new StreamReader(fullPath) /*new FileStream(fullPath, FileMode.OpenOrCreate)*/)
                //{
                //    string result = stream.ReadToEnd();
                //    Console.WriteLine(result);
                //}


                using (BinaryWriter stream = new BinaryWriter(File.OpenWrite(fullPath)))
                {
                    User user = new User
                    {
                        ID = 1,
                        Login = "admin",
                        Password = "123asd"
                    };

                    stream.Write(user.ID);
                    stream.Write(user.Login);
                    stream.Write(user.Password);
                }

                using (BinaryReader stream = new BinaryReader(File.OpenRead(fullPath)) /*new FileStream(fullPath, FileMode.OpenOrCreate)*/)
                {
                    User user = new User();

                    user.ID = stream.ReadInt32();
                    user.Login = stream.ReadString();
                    user.Password = stream.ReadString();
                }
            }
            else
            {
                Console.WriteLine("Wrong number");
                return;
            }
            Console.ReadLine();

        }
    }
}
